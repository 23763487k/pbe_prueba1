# Según los conocimientos entregados en clase, el alumno deberá:
# Crear un repositorio GIT público para hacer seguimiento de la aplicación a crear.
# Debe crear una app en Python Flask.
# Debe crear una ruta que reciba los siguientes datos por GET y POST:
# Vehículo,Año,Marca,Color,Combustible,NumPuertas,Tracción
# Cuando la ruta reciba el conjunto de datos, estos deben guardarse en un archivo CSV separado por ";". 
# Ejemplo:
# 2023;TOYOTA;ROJO;DIESEL;4;4x4;
# 2024;TESLA;NEGRO;ELÉCTRICO;4;DELANTERA;
# Debe entregar al final de la clase la dirección del repositorio público creado en primera instancia.

from flask import Flask
from flask import request
from flask import render_template
import os

app = Flask(__name__)

@app.route('/ingreso', methods=['GET'])     # Pagina Inicial
def login():
    return render_template('ingreso.html')

@app.route('/vehiculo', methods=['POST'])   # Deriva a formulario de Ingreso de vehiculos
def dologin():
    email = request.form['email']
    return render_template('vehiculo.html', email=email) 

@app.route('/guardarVehiculo', methods=['GET', 'POST']) # Toma los datos ingresados y guarda en documento CSV
def guardarVehiculo():
    if request.method == 'GET':
        vehiculo = request.args.get('vehiculo', 'no proporcionado')
        year = request.args.get('year', 'no proporcionado')
        marca = request.args.get('marca', 'no proporcionado')
        color = request.args.get('color', 'no proporcionado')
        combustible = request.args.get('combustible', 'no proporcionado')
        numPuertas = request.args.get('numPuertas', 'no proporcionado')
        traccion = request.args.get('traccion', 'no proporcionado')
    else:
        vehiculo = request.form['vehiculo']
        year = request.form['year']
        marca = request.form['marca']
        color = request.form['color']
        combustible = request.form['combustible']
        numPuertas = request.form['numPuertas']
        traccion = request.form['traccion']
        
    datos = f"{vehiculo};{year};{marca};{color};{combustible};{numPuertas};{traccion};\n"

    carpeta = 'data'                    
    if not os.path.exists(carpeta):                                         # Crea carpeta Data si no existiera
        os.mkdir(carpeta)
    vehicles_file = os.path.join(carpeta, 'vehiculos.csv')
    if not os.path.exists(vehicles_file):
        with open(vehicles_file, 'w', newline='') as f:
            f.write('Vehiculo;Anio;Marca;Color;Combustible;NumPuertas;Traccion;\n')
    with open(vehicles_file, 'a') as f:                                     # Guarda los Datos tomados
        f.write(datos)
    
    return render_template('vehiculo.html', message="Vehículo guardado!") 

if __name__ == '__main__':
    app.run(debug=True)